# Guia-tour

## Introdução

  O projeto Guia Tour pretende contribuir com a renda dos estados Brasileiros, tornando o mesmo mais atrativos para os turistas pois com uma rota melhor traçada, os riscos de assaltos contra turistas irá diminuir, além de otimizar a experiência dos visitantes nos estados trazendo interatividade e tornando um guia de bolso.

## Funcionalidades

  - Criação de Roteiros
  - Criação de linha de tempo para as atividades
  - Notificação do próximo lugar de visita da linha do tempo
  - Filtro baseado na localização do usuário
  - Pesquisas de lugares

// TODO 

 
 ## Tecnologias
   - Angular
   - Ionic 4
   - Firebase
   - GoogleMaps
   - GooglePlaces
   - DialogFlow

## Imagens de Demostração do App

// TODO
