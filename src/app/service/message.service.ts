import { ToastController, LoadingController } from '@ionic/angular';
import { Injectable } from '@angular/core';


@Injectable({
  providedIn: 'root'
})
export class MessageService {
  isLoading = false;
  constructor(
    private toastCtrl: ToastController, public loadingController: LoadingController) { }

  showToast(msg, color) {
    this.toastCtrl.create({
      message: msg,
      color: color,
      duration: 2000
    }).then(toast => toast.present());
  }

  async present(msg:string) {
    this.isLoading = true;
    return await this.loadingController.create({
      message: msg
    }).then(a => {
      a.present().then(() => {
        if (!this.isLoading) {
          a.dismiss().then();
        }
      });
    });
  }

  async dismiss() {
    this.isLoading = false;
    return await this.loadingController.dismiss().then();
  }




}
