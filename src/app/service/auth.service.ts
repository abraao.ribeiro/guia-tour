import {AngularFirestore,AngularFirestoreDocument} from "@angular/fire/firestore";
import { AngularFireAuth } from "@angular/fire/auth";
import { Injectable } from "@angular/core";
import { Router } from "@angular/router";
import * as firebase from "firebase/app";
import { Observable} from "rxjs";

import { MessageService } from "./message.service";
import { User } from "./../model/user";
@Injectable({
  providedIn: "root"
})
export class AuthService {
  user: Observable<any>;
  displayName;
  constructor(
    private firebaseauth: AngularFireAuth,
    private angularFireStore: AngularFirestore,
    private messageService: MessageService,
    private router: Router
  ) {}

  loginComEmail(value) {
    return new Promise((resolve, reject) => {
      this.firebaseauth.auth
        .signInWithEmailAndPassword(value.email, value.password)
        .then(credential => {
          res => resolve(res);
          this.messageService.showToast("Login realizado com sucesso","success");
          console.log(credential);
          this. authState();
        })
        .catch((erro: any) => {
          this.messageService.showToast("Cadastro não encontrado","warning");
          reject(erro);
        });
    });
  }

  cadastrarUsuario(value): Promise<any> {
    return new Promise((resolve, reject) => {
      this.firebaseauth.auth
        .createUserWithEmailAndPassword(value.email, value.password)
        .then(credential => {
          this.updateUserData(credential.user)
          this.messageService.showToast("Usuário cadastrado com sucesso ","success");
          resolve(credential)
        }).catch((erro: any) => {
          this.messageService.showToast("O endereço de e-mail já foi cadastrado","warning");
          console.log(erro);
        });
    });
  }

  googleLogin() {
    const provider = new firebase.auth.GoogleAuthProvider();
    return this.oAuthLogin(provider);
  }
  
  signOut() {
    this.firebaseauth.auth.signOut().then(() => {
      this.router.navigate(["/"]);
    });
  }
  

  private oAuthLogin(provider) {
    return this.firebaseauth.auth.signInWithPopup(provider).then(credential => {
      this.updateUserData(credential.user);
    });
  }

  private updateUserData(user) {
    // Sets user data to firestore on login
    const userRef: AngularFirestoreDocument<any> = this.angularFireStore.doc(
      `users/${user.uid}`
    );
    const data: User = {
      uid: user.uid,
      email: user.email,
      displayName: user.displayName,
      photoURL: user.photoURL
    };
    return userRef.set(data, { merge: true });
  }

  userDetails(): Promise<any> {
    let user;
    return new Promise((resolve, reject) => {
      user = this.firebaseauth.auth.currentUser;
      resolve(user), err => reject(err);
    });
  }

  authState(){
    this.firebaseauth.authState.subscribe(state => {
      if (state) {
        this.router.navigate(["/tabs/home"]);
      } else {
        this.router.navigate(["/login"]);
      }
    });
  }
  
}
