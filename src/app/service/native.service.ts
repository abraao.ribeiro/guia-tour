import { Injectable } from "@angular/core";
import { DatePicker } from "@ionic-native/date-picker/ngx";

import { Roteiro } from "../model/roteiro";
import { Atividade } from "../model/atividade";
import { LocalNotifications } from "@ionic-native/local-notifications/ngx";

@Injectable({
  providedIn: "root"
})
export class NativeService {
  constructor(
    private datePicker: DatePicker,
    private localNotifications: LocalNotifications
  ) {}

  /**
   * Salva a Data do datePicker e atribui o valor a atividade.data
   * @param atividade 
   */
  datePickerDate(atividade: Atividade) {
    this.datePicker
      .show({
        date: new Date(),
        mode: "datetime",
        androidTheme: this.datePicker.ANDROID_THEMES.THEME_DEVICE_DEFAULT_LIGHT
      })
      .then(date => {
        atividade.data = date;
        err => console.log("Error occurred while getting date: ", err);
      });
  }

  /**
   * Salva a hora do datePicker e atribui o valor a Atividade.horaEntrada
   * @param atividade 
   */
  timeHoraEntrada(atividade: Atividade) {
    this.datePicker
      .show({
        date: new Date(),
        mode: "time",
        androidTheme: this.datePicker.ANDROID_THEMES.THEME_DEVICE_DEFAULT_DARK
      })
      .then(date => {
        atividade.horaEntrada = date;
        err => console.log("Error occurred while getting date: ", err);
      });
  }
  
  /**
   * Salva a hora do datePicker e atribui o valor a Atividade.horaSaida 
   * @param atividade 
   */
  timeHoraSaida(atividade: Atividade) {
    this.datePicker
      .show({
        date: new Date(),
        mode: "time",
        androidTheme: this.datePicker.ANDROID_THEMES.THEME_DEVICE_DEFAULT_DARK
      })
      .then(date => {
        atividade.horaSaida = date;
        err => console.log("Error occurred while getting date: ", err);
      });
  }

  /**
   * Cria uma notificao local para exibir para o usuario
   * @param titulo 
   * @param date 
   * @param text 
   */
  localNotification(titulo:string, dateTime:Date,  text:string, img:string) {
    this.localNotifications.schedule({
      title: titulo,
      text: text,
      trigger: { at: dateTime},
      attachments: [img],
      led: "FF0000",
      sound: null,
      foreground:true
    });
  }
  notificationsLocation(lat?:any, lng?:any , img?:string){
    this.localNotifications.schedule({
      title: 'Você  está próximo do seu destino',
      led: "FF0000",
      sound: null,
      attachments: [img],
      foreground:true,
      trigger: {
          type: 'location',
          center: [lat, lng],
          radius: 15,
          notifyOnEntry: true
      },

    })
  }
  

}
