import { Injectable, NgZone } from "@angular/core";
import { Geolocation } from "@ionic-native/geolocation/ngx";
declare let google: any;
@Injectable({
  providedIn: "root"
})
export class GooglePlacesService {
  GooglePlaces: any;
  popoverData: any;
  GoogleAutocomplete;
  constructor(public zone: NgZone, public geolocation: Geolocation) {
    let elem = document.createElement("div");
    this.GooglePlaces = new google.maps.places.PlacesService(elem);
    this.GoogleAutocomplete = new google.maps.places.AutocompleteService();
  }

  /**
   * Pegando a posição atual do usuario
   * @param radius
   * @param type
   */
  public async userGeolocation(radius: any, type: any): Promise<any> {
    
    let myLatlng: string;
    const currentPosition = await this.geolocation.getCurrentPosition();

    myLatlng = new google.maps.LatLng(
      currentPosition.coords.latitude,
      currentPosition.coords.longitude,
      currentPosition.coords.accuracy
    );

    return myLatlng;
  }

  /**
   * Resultado com base na localização do usuario
   *
   * @param radius
   * @param type
   */
  public async getPlaces(
    radius?: any,
    type?: any,
    page?: any
  ): Promise<any> {
    const userLocation = await this.userGeolocation(radius, type);
    return new Promise((resolve, reject) => {
      this.GooglePlaces.nearbySearch(
        {
          location: userLocation,
          radius: radius,
          type: type,
          laguages: "pt-BR"
        },
        (result, status, pagination) => {
          let places = [];
          if (status === "OK") {
            this.zone.run(() => {
              for (const place of result) {
                places.push(place);
              }
              resolve(places);
              if (pagination.hasNextPage) {
                page = pagination.nextPage();
              }
            });
          }
        }
      );
    });
  }

  /**
   * Pegando os Detalhes de um lugar especifico
   * @param myLatlng
   * @param placeId
   */

  public async getPlaceDetail(myLatlng: string, placeId: string): Promise<any> {
    const userLocation = await this.userGeolocation(myLatlng, placeId);
    return new Promise((resolve, reject) => {
      this.GooglePlaces.getDetails(
        {
          location: userLocation,
          laguages: "pt-BR",
          placeId: placeId,
          fields: [
            "name",
            "geometry",
            "photos",
            "rating",
            "reviews",
            "price_level",
            "url",
            "scope",
            "user_ratings_total",
            "utc_offset",
            "alt_id",
            "formatted_phone_number",
            "opening_hours",
            "formatted_address"
          ]
        },
        near_places_details => {
          resolve(near_places_details);
        }
      );
    });
  }

  /**
   * Busca de com autocomplete de lugares
   */

  public async updateSearchResults(input): Promise<any> {
    let autocompleteItems = [];
    return new Promise((resolve, reject) => {
      this.GoogleAutocomplete.getPlacePredictions(
        {
          input: input,
          componentRestrictions: { country: "br" }
        },
        (predictions, status) => {
          if (predictions) {
            this.zone.run(() => {
              predictions.forEach(prediction => {
                autocompleteItems.push(prediction);
              });
              resolve(autocompleteItems);
              console.log(autocompleteItems);
              autocompleteItems = [];
            });
          }
        }
      );
    });
  }
}
