import { Injectable } from "@angular/core";
import {
  AngularFirestore,
  AngularFirestoreCollection
} from "@angular/fire/firestore";

import { Observable } from "rxjs";
import { map, take, filter, switchMap } from "rxjs/operators";

import { SecurityService } from "./security.service";
import { Roteiro } from "./../model/roteiro";

@Injectable({
  providedIn: "root"
})
export class RoteiroService {
  roteiro: Roteiro;

  constructor(
    private angularFireStore: AngularFirestore,
    private securityService: SecurityService
  ) { 
    this.getFireCollection(); 
  }

  private getFireCollection(): AngularFirestoreCollection<Roteiro> {
    return this.angularFireStore.collection<Roteiro>("roteiros", ref =>
      ref.where("userId", "==", this.securityService.getUserId())
    );
  }

  findAllRoteiro(): Observable<Roteiro[]> {
    return this.getFireCollection()
      .snapshotChanges()
      .pipe(
        map(actions => {
          return actions.map(a => {
            const data = a.payload.doc.data();
            const id = a.payload.doc.id;
            return { id, ...data };
          });
        })
      );
  }

  getRoteiroId(id: string): Observable<Roteiro> {
    return this.getFireCollection()
      .doc<Roteiro>(id)
      .valueChanges()
      .pipe(
        take(1),
        map(roteiro => {
          roteiro.id = id;
          return roteiro;
        })
      );
  }

  criarRoteiro(roteiro: Roteiro) {
    const id = this.angularFireStore.createId();
    const item: Roteiro = {
      titulo: roteiro.titulo,
      descricao: roteiro.descricao,
      atividade: roteiro.atividade,
      userId: this.securityService.getUserId()
    };
    this.getFireCollection()
      .doc(id)
      .set(item);
  }

  atualizarRoteiro(roteiro: Roteiro): Promise<void> {
    console.log(roteiro);
    return this.getFireCollection()
      .doc(roteiro.id)
      .update({
        titulo: roteiro.titulo,
        descricao: roteiro.descricao,
        atividade: roteiro.atividade
      });
  }

  criarAtividade(roteiro: Roteiro) {
    const item: Roteiro = {
      titulo: roteiro.titulo,
      descricao: roteiro.descricao,
      atividade: roteiro.atividade,
      userId: this.securityService.getUserId()
    };
    this.getFireCollection()
      .doc(roteiro.id)
      .set(item);
  }

  deleteRoteiro(id) {
    return this.getFireCollection()
      .doc<Roteiro>(id)
      .delete();
  }

 searchRoteiroTitulo(searchValue){
    return this.angularFireStore.collection('roteiros',ref => ref.where('titulo', '>=', searchValue)
      .where('titulo', '<=', searchValue + '\uf8ff'))
      .snapshotChanges()
}

  setRoteiro(roteiro: Roteiro) {
    this.roteiro = roteiro;
  }
  getRoteiro() {
    return this.roteiro;
  }
}
