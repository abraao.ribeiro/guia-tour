import { CardCustomModule } from 'src/app/shared/components/card-custom/card-custom.module';
import { AngularFirestoreModule, FirestoreSettingsToken } from "@angular/fire/firestore";
import { BrowserModule, HAMMER_GESTURE_CONFIG } from "@angular/platform-browser";
import { IonicModule, IonicRouteStrategy } from "@ionic/angular";
import { NgModule, APP_INITIALIZER } from "@angular/core";
import { AngularFireStorageModule } from "@angular/fire/storage";
import { AngularFireAuthModule } from "@angular/fire/auth";
import { RouteReuseStrategy } from "@angular/router";
import { registerLocaleData } from '@angular/common';
import { HttpClientModule } from "@angular/common/http";
import { LocalNotifications } from "@ionic-native/local-notifications/ngx";
import { AngularFireModule } from "@angular/fire";
import { FormsModule } from "@angular/forms";
import { SplashScreen } from "@ionic-native/splash-screen/ngx";
import { DatePicker } from "@ionic-native/date-picker/ngx";
import { StatusBar } from "@ionic-native/status-bar/ngx";
import { IonicStorageModule } from '@ionic/storage';
import localePt from '@angular/common/locales/pt';
import { Geolocation } from '@ionic-native/geolocation/ngx';

import { AtividadeEditPageModule } from './pages/atividade/atividade-edit/atividade-edit.module';
import { RoteiroEditPageModule } from './pages/roteiro/roteiro-edit/roteiro-edit.module';
import { ServiceWorkerModule } from '@angular/service-worker';
import { IonicGestureConfig } from "./util/IonicGestureConfig";
import { PopoverPageModule } from "./pages/popover/popover.module";
import { AtividadeEditPage } from "./pages/atividade/atividade-edit/atividade-edit.page";
import { AppRoutingModule } from "./app-routing.module";
import { RoteiroEditPage } from './pages/roteiro/roteiro-edit/roteiro-edit.page';
import { LoginPageModule } from "./pages/user/login/login.module";
import { SecurityService } from "./service/security.service";
import { LugaresComponent } from "./shared/components/lugares/lugares.component";
import { AppComponent } from "./app.component";
import { LugaresModule } from "./shared/components/lugares/lugares.module";
import { PopoverPage } from "./pages/popover/popover.page";
import { environment } from "../environments/environment";
import { AuthService } from "./service/auth.service";
import { AuthGuard } from "./service/auth-guard";
import { PlacesSearchModule } from './shared/components/places-search/places-search.module';

registerLocaleData(localePt, 'pt-BR');
export function securityInit(securityService: SecurityService) {
  return () => securityService.init();
}

@NgModule({
  declarations: [AppComponent],
  entryComponents: [
    LugaresComponent,
    AtividadeEditPage,
    RoteiroEditPage,
    PopoverPage],

  imports: [
    BrowserModule,
    LugaresModule,
    FormsModule,
    PlacesSearchModule,
    HttpClientModule,
    LoginPageModule,
    AppRoutingModule,
    IonicStorageModule.forRoot({
      name: 'guia_db'
    }),
    PopoverPageModule,
    IonicModule.forRoot(),
    AtividadeEditPageModule,
    RoteiroEditPageModule,
    CardCustomModule,
    AngularFirestoreModule,
    AngularFireAuthModule,
    AngularFireStorageModule,
    AngularFireModule.initializeApp(environment.firebase),
    AngularFirestoreModule.enablePersistence(),
    ServiceWorkerModule.register('ngsw-worker.js', { enabled: environment.production })
  ],
  providers: [
    StatusBar,
    SplashScreen,
    SecurityService,
    AuthService,
    AuthGuard,
    DatePicker,
    Geolocation,
    LocalNotifications,
    {
      provide: APP_INITIALIZER,
      useFactory: securityInit,
      deps: [SecurityService],
      multi: true
    },
    {
      provide: HAMMER_GESTURE_CONFIG,
      useClass: IonicGestureConfig
    },
    { provide: RouteReuseStrategy, useClass: IonicRouteStrategy },
    { provide: FirestoreSettingsToken, useValue: {} }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
