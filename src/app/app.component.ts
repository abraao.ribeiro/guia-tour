import { AuthService } from './service/auth.service';
import { Component, ViewChild, ElementRef } from "@angular/core";
import { SplashScreen } from "@ionic-native/splash-screen/ngx";
import { StatusBar } from "@ionic-native/status-bar/ngx";
import { Platform } from "@ionic/angular";
import { Router } from "@angular/router";
@Component({
  selector: "app-root",
  templateUrl: "app.component.html"
})
export class AppComponent {
  @ViewChild("scrollMe") private myScrollContainer: ElementRef;

  constructor(
    private platform: Platform,
    private splashScreen: SplashScreen,
    private statusBar: StatusBar,
    private auth: AuthService,
    private router: Router
  ) {
    this.initializeApp();
  }

  initializeApp() {
    this.platform.ready().then(() => {
      this.statusBar.styleLightContent();
      this.splashScreen.hide();
    });
    this.auth.authState();
  }
}
