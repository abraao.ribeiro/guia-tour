import { Input } from '@angular/core';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-places',
  templateUrl: './places.component.html',
  styleUrls: ['./places.component.scss']
})
export class PlacesComponent implements OnInit {
  @Input() defaultImage: string = "assets/img/placeholder.png";
  @Input() place: any;
  constructor() { }

  ngOnInit() {
  }
  @Input()
  getImagens(imagens: any): string {
    let url: string;
    if (imagens) {
      imagens.forEach(imagem => {
        url = imagem.getUrl();
        return
      });
    } else {
      return
    }
    return url;
  }

}
