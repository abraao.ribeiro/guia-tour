import { CardCustomModule } from './../card-custom/card-custom.module';
import { LazyLoadImageModule } from 'ng-lazyload-image';
import { IonicModule } from '@ionic/angular';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PlacesComponent } from './places.component';

@NgModule({
  imports: [
    CommonModule,
    IonicModule,
    CardCustomModule,
    LazyLoadImageModule,
  ],
  declarations: [PlacesComponent],
  exports: [PlacesComponent]
})
export class PlacesModule { }
