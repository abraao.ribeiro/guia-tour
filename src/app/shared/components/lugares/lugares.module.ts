import { CardCustomModule } from '../card-custom/card-custom.module';
import { FormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LugaresComponent } from './lugares.component';
import { RouterModule } from '@angular/router';
import { LazyLoadImageModule } from 'ng-lazyload-image';
@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule,
    CardCustomModule,
    LazyLoadImageModule,
  ],
  declarations: [LugaresComponent],
  exports: [LugaresComponent,LazyLoadImageModule,CardCustomModule]
})
export class LugaresModule { }
