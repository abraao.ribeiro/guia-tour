import { Component, OnInit, Input } from "@angular/core";

@Component({
  selector: "app-lugares.component",
  templateUrl: "./lugares.component.html",
  styleUrls: ["./lugares.component.scss"]
})
export class LugaresComponent implements OnInit {

  @Input() defaultImage: string = "assets/img/placeholder.png";
  @Input() nearbyItems: any [];

  constructor() { }

  ngOnInit() {
  }

  /* @Input()
  fechaModal(value) {
    this.modalController.dismiss({
      result: value
    });
  } */
}