import { GooglePlacesService } from 'src/app/service/google-places.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-places-search',
  templateUrl: './places-search.component.html',
  styleUrls: ['./places-search.component.scss']
})
export class PlacesSearchComponent implements OnInit {
  searchInput: any;
  places: any[];
  constructor(private googlePlacesService: GooglePlacesService) { }

  ngOnInit() {
  }


  search() {
    this.googlePlacesService.updateSearchResults(this.searchInput).then(res => {
      this.places = res;
    })
  }

}
