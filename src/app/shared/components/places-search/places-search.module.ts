import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PlacesSearchComponent } from './places-search.component';
import { IonicModule } from '@ionic/angular';
import { PlacesModule } from '../places/places.module';

@NgModule({
  imports: [
    CommonModule,
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule,
    PlacesModule
  ],
  declarations: [PlacesSearchComponent],
  exports: [PlacesSearchComponent]
})
export class PlacesSearchModule { }
