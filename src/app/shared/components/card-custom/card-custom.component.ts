import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-card-custom',
  templateUrl: './card-custom.component.html',
  styleUrls: ['./card-custom.component.scss']
})
export class CardCustomComponent implements OnInit {
  @Input() place: any;
  @Input() defaultImage: string = "assets/img/placeholder.png";
  constructor() { }

  ngOnInit() {
    this.place
  }

  @Input()
  getImagens(imagens: any): string {
    let url: string;
    if (imagens) {
      imagens.forEach(imagem => {
        url = imagem.getUrl();
        return
      });
    } else {
      return
    }
    return url;
  }


}
