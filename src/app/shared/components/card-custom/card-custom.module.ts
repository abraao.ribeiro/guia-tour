import { IonicModule } from '@ionic/angular';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CardCustomComponent } from './card-custom.component';
import { StarRating } from 'ionic4-star-rating';

import { LazyLoadImageModule, intersectionObserverPreset } from 'ng-lazyload-image';

@NgModule({
  imports: [
    CommonModule,
    IonicModule,
    LazyLoadImageModule.forRoot({
      preset: intersectionObserverPreset
    }),
  ],
  exports: [CardCustomComponent, StarRating],
  declarations: [CardCustomComponent, StarRating]
})

export class CardCustomModule { }
