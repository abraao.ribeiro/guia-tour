import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { TutorialGuard } from './shared/guards/tutorial.guard';
import { PlacesSearchComponent } from './shared/components/places-search/places-search.component';

const routes: Routes = [
  { path: '', loadChildren: './pages/tabs/tabs.module#TabsPageModule',canActivate: [TutorialGuard]},
  { path: 'atividade-list/:id', loadChildren: './pages/atividade/atividade-list/atividade-list.module#AtividadeListPageModule' },
  { path: 'atividade-edit', loadChildren: './pages/atividade/atividade-edit/atividade-edit.module#AtividadeEditPageModule' },
  { path: 'atividade-edit/:id', loadChildren: './pages/atividade/atividade-edit/atividade-edit.module#AtividadeEditPageModule' },
  { path: 'roteiro-edit/:id', loadChildren: './pages/roteiro/roteiro-edit/roteiro-edit.module#RoteiroEditPageModule' },
  { path: 'roteiro-edit', loadChildren: './pages/roteiro/roteiro-edit/roteiro-edit.module#RoteiroEditPageModule' },
  { path: 'lugar-list', loadChildren: './pages/lugar/lugar-list/lugar-list.module#LugarListPageModule'},
  { path: 'lugar-detail/:id', loadChildren: './pages/lugar/lugar-detail/lugar-detail.module#LugarDetailPageModule' },
  { path: 'chatbot', loadChildren: './pages/chatbot/chatbot.module#ChatbotPageModule' },
  { path: 'login', loadChildren: './pages/user/login/login.module#LoginPageModule', canActivate: [TutorialGuard] },
  { path: 'register', loadChildren: './pages/user/register/register.module#RegisterPageModule' },
  { path: 'tutorial', loadChildren: './pages/tutorial/tutorial.module#TutorialPageModule' },
  { path: 'places-search', component: PlacesSearchComponent}

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule { }
1
