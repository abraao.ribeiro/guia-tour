import { LugaresComponent } from '../../shared/components/lugares/lugares.component';
import { ViewChild } from '@angular/core';
import { GooglePlacesService } from './../../service/google-places.service';
import { Component, OnInit } from '@angular/core';

import { IonInfiniteScroll, LoadingController, ModalController } from '@ionic/angular';
import { MessageService } from 'src/app/service/message.service';
@Component({
  selector: 'app-home',
  templateUrl: './home.page.html',
  styleUrls: ['./home.page.scss'],
})

export class HomePage implements OnInit {

  @ViewChild(IonInfiniteScroll) infiniteScroll: IonInfiniteScroll;
  pagination: any;
  places: any[] = new Array();
  constructor(
    private googlePlaceService: GooglePlacesService,
    public loadingController: LoadingController,
    private modalController: ModalController,
    private messageService: MessageService) {
  }

  ngOnInit() {
    this.nearbyPlace('500', '')
  }
  ionViewDidEnter() {
    //this.getPlaces();
  }

  async nearbyPlace(km, type) {
    const loading = await this.loadingController.create({
      message: 'Aguarde ...',
    });
    await loading.present();
    const data = await this.googlePlaceService.getPlaces(km, type, this.pagination);
    this.places = await data;
    await Promise.all(data).then(() => {
      loading.dismiss();
    })
  }

  /*   async getPlaces() {
      let location = await this.getLocation();
      this.googlePlaces.nearbySearch({
        location: location,
        radius: 500,
        types: ['places']
      }, (result, status, pagination) => {
        this.zone.run(() => {
          if (status === 'OK') {
            for (const place of result) {
              this.places.push(place);
            }
            console.log(result);
            if (pagination.hasNextPage) {
              this.pagination = pagination.nextPage();
            }
            console.log('Finished!');
          }
        })
      })
    } */

  /*  async getLocation() {
     const location = await this.geolocation.getCurrentPosition();
     let mylocation = new google.maps.LatLng(location.coords.latitude, location.coords.longitude);
     return mylocation;
   } */

  loadData(event) {
    setTimeout(() => {
      event.target.complete();
      if (this.pagination == true) {
        event.target.disabled = true;
      }
    }, 500);

  }

  toggleInfiniteScroll() {
    this.infiniteScroll.disabled = !this.infiniteScroll.disabled;
  }



  async lugarModal() {
    const modal = await this.modalController.create({
      component: LugaresComponent
    })
    await modal.present();
  }

}
