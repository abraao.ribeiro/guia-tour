import { AuthService } from './../../../service/auth.service';
import { Component, OnInit } from '@angular/core';
import { User } from 'firebase';

@Component({
  selector: 'app-account',
  templateUrl: './account.page.html',
  styleUrls: ['./account.page.scss'],
})
export class AccountPage implements OnInit {

  user: User
  constructor(private authService: AuthService) { }

  ngOnInit() {
    this.userDetail();

  }

  userDetail() {
    this.authService.userDetails().then((user) => {
      this.user = user;
      console.log(user);
    });

  }


  sair() {
    this.authService.signOut();
  }
}
