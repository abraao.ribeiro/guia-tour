import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { Component, OnInit, ViewChild } from '@angular/core';

import { AuthService } from './../../../service/auth.service';

@Component({
  selector: "app-login",
  templateUrl: "./login.page.html",
  styleUrls: ["./login.page.scss"]
})
export class LoginPage implements OnInit {
  formulario: FormGroup;
  errorMessage: string;
  constructor(private auth: AuthService, private formBuilder: FormBuilder) {}

  ngOnInit() {
    this.formulario = this.formBuilder.group({
      email: [null, [Validators.required, Validators.email]],
      password: [null, [Validators.required, Validators.minLength(6)]]
    });
  }

  loginUsuario(value) {
    this.auth.loginComEmail(value);
  }

  cadastrar(value) {
    this.auth.cadastrarUsuario(value);
  }

  loginGoogle() {
    this.auth.googleLogin();
  }
  
  validation_messages = {
    email: [
      { type: "required", message: "O campo email deve ser preenchido." },
      { type: "pattern", message: "Por favor digite um email válido." }
    ],
    password: [
      { type: "required", message: "Senha requerida." },
      {
        type: "minlength",
        message: "A senha deve ter pelo menos 6 caracteres."
      }
    ]
  };
}
