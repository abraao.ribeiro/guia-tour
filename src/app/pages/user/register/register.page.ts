import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Component, OnInit } from '@angular/core';
import { AuthService } from 'src/app/service/auth.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.page.html',
  styleUrls: ['./register.page.scss'],
})
export class RegisterPage implements OnInit {
  formulario: FormGroup;
  errorMessage: string;
  constructor(private auth: AuthService, private formBuilder: FormBuilder) {}

  ngOnInit() {
    this.formulario = this.formBuilder.group({
      email: [null, [Validators.required, Validators.email]],
      password: [null, [Validators.required, Validators.minLength(6)]]
    });
  }

  cadastrar(value) {
    this.auth.cadastrarUsuario(value);
  }

  validation_messages = {
    email: [
      { type: "required", message: "O campo email deve ser preenchido." },
      { type: "pattern", message: "Por favor digite um email válido." }
    ],
    password: [
      { type: "required", message: "Senha requerida." },
      {
        type: "minlength",
        message: "A senha deve ter pelo menos 6 caracteres."
      }
    ]
  };
}