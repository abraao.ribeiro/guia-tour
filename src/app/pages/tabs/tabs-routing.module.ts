import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { TabsPage } from './tabs.page';
import { TutorialGuard } from 'src/app/shared/guards/tutorial.guard';

const routes: Routes = [
  {
    path: 'tabs',
    component: TabsPage,
    children: [
      {
        path: 'roteiro',
        children: [
          {
            path: '',
            loadChildren:
              '../roteiro/roteiro-list/roteiro-list.module#RoteiroListPageModule',
              canActivate: [TutorialGuard]
          }
        ]
      },
      {
        path: 'lugar',
        children: [
          {
            path: '',
            loadChildren: '../lugar/lugar-list/lugar-list.module#LugarListPageModule'
          }
        ]
      },
      {
        path: 'home',
        children: [
          {
            path: '',
            loadChildren: '../home/home.module#HomePageModule'
          }
        ]
      },
      {
        path: 'rota',
        children: [
          {
            path: '',
            loadChildren: '../rota/rota.module#RotaPageModule'
          }
        ]
      },
      {
        path: 'account',
        children: [
          {
            path: '',
            loadChildren: '../user/account/account.module#AccountPageModule'
          }
        ]
      },
      {
        path: 'tutorial',
        children: [
          {
            path: '',
            loadChildren: '../tutorial/tutorial.module#TutorialPageModule'
          }
        ]
      },

      {
        path: '',
        redirectTo: '/tabs/roteiro',
        pathMatch: 'full'
      }
    ]
  },
  {
    path: '',
    redirectTo: '/tabs/roteiro',
    pathMatch: 'full'
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TabsPageRoutingModule { }
