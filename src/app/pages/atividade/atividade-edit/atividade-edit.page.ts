import { MessageService } from 'src/app/service/message.service';
import { Router } from '@angular/router';
import { ActivatedRoute } from '@angular/router';
import { NativeService } from 'src/app/service/native.service';
import { ModalController, LoadingController } from '@ionic/angular';
import { Component, OnInit } from '@angular/core';

import { RoteiroService } from 'src/app/service/roteiro.service';
import { Atividade } from 'src/app/model/atividade';
import { Roteiro } from 'src/app/model/roteiro';
import { LugaresComponent } from 'src/app/shared/components/lugares/lugares.component';
import { DatePipe } from '@angular/common';

@Component({
  selector: 'app-atividade-edit',
  templateUrl: './atividade-edit.page.html',
  styleUrls: ['./atividade-edit.page.scss'],
  providers: [DatePipe]
})
export class AtividadeEditPage implements OnInit {

  nearbyItem: any;
  roteiro: Roteiro = {
    titulo: '',
    descricao: '',
    atividade: []
  }
  atividade: Atividade = {
    lugar: '',
    data: null,
    horaEntrada: null,
    horaSaida: null,

  }
  image: string = "assets/img/logo.png";

  constructor(
    private modalController: ModalController,
    private roteiroService: RoteiroService,
    private activatedRoute: ActivatedRoute,
    private messageService: MessageService,
    private router: Router,
    private datePipe: DatePipe,
    private nativeService: NativeService) { }

  ngOnInit() {
    this.findAtividadeId();
  }

  findAtividadeId() {
    let id = this.activatedRoute.snapshot.params['id'];

    if (id) {
      this.roteiroService.getRoteiroId(id)
        .subscribe(r => {
          this.roteiro = r;
          console.log(this.roteiro);

        });
    }
  }
  atualizarRoteiro() {
    this.roteiroService.atualizarRoteiro(this.roteiro).then(() => {
      this.router.navigateByUrl('tabs/roteiro')
      this.messageService.showToast('Atividade atualizado com sucesso', "success");
    }, err => {
      this.messageService.showToast('Erro', "danger");
    });
  }

  async abrirLugar() {
    const modal = await this.modalController.create({
      component: LugaresComponent

    });
    await modal.present();
    this.nearbyItem = await modal.onDidDismiss();
    this.atividade.img = this.getImagens(this.nearbyItem.data.result.photos);
    this.atividade.reference = this.nearbyItem.data.result.reference;
    if (this.nearbyItem.data.result.name) {
      this.atividade.lugar = this.nearbyItem.data.result.name;
    } else {
      this.atividade.lugar = this.nearbyItem.data.result.terms[0].value;
    }
    console.log("AtividadeEditPage", "atividades:", this.nearbyItem);

  }

  novaAtividade() {
    this.datePipe.transform(this.atividade.data, 'dd-MM-yyyy')
    this.roteiro.atividade.push(this.atividade);
    this.notificacaoPorData();
    this.roteiroService.atualizarRoteiro(this.roteiro);
    this.router.navigate(['atividade-list', this.roteiro.id]);
    this.messageService.showToast("Atividade criada com sucesso", "success")
  }

  notificacaoPorData() {
    this.nativeService.localNotification(
      this.atividade.lugar,
      this.atividade.data,
      'Lembre-se de seu próximo local de visita \n' + this.atividade.data,
      this.atividade.img);
  }

  notificacaoPorLocal() {
    this.nativeService.notificationsLocation(
      this.atividade.lat,
      this.atividade.lng,
      this.atividade.img);
  }

  getImagens(imagens: any): string {
    let url: string;
    if (imagens) {
      imagens.forEach(imagem => {
        url = imagem.getUrl();
        return
      });
    } else {
      return this.image;
    }
    return url;
  }
  timeHoraEntrada() {
    this.nativeService.timeHoraEntrada(this.atividade);
  }
  timeHoraSaida() {
    this.nativeService.timeHoraSaida(this.atividade);
  }
  showDatePicker() {
    this.nativeService.datePickerDate(this.atividade);
  }
}
