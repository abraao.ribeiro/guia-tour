import { ActionSheetController, LoadingController } from '@ionic/angular';
import { ActivatedRoute, Router } from '@angular/router';
import { Component } from '@angular/core';

import { MessageService } from 'src/app/service/message.service';
import { RoteiroService } from './../../../service/roteiro.service';
import { Roteiro } from 'src/app/model/roteiro';

@Component({
  selector: 'app-atividade-list',
  templateUrl: './atividade-list.page.html',
  styleUrls: ['./atividade-list.page.scss'],
})
export class AtividadeListPage {


  roteiro: Roteiro = {
    atividade: []
  };
  constructor(
    private roteiroService: RoteiroService,
    private activatedRoute: ActivatedRoute,
    private messageService: MessageService,
    private loadingController: LoadingController,
    public actionSheetController: ActionSheetController,
    private router: Router) {
  }


  ionViewDidEnter() {
    this.findAtividadeId();
  }

  async findAtividadeId() {
    const loading = await this.loadingController.create({
      message: "Carregando as Atividades..."
    });
    await loading.present();
    let id = this.activatedRoute.snapshot.params['id'];
    if (id) {
      this.roteiroService.getRoteiroId(id)
        .subscribe(res => {
          this.roteiro = res;
          loading.dismiss();
        });
    }
  }

  async abrirAtividadeEdit() {
    this.router.navigateByUrl('atividade-edit')
    this.router.navigate(['atividade-edit', this.roteiro.id])
  }

  async presentActionSheet() {
    const actionSheet = await this.actionSheetController.create({
      header: 'Ações',
      buttons: [{
        text: 'Delete',
        role: 'destructive',
        icon: 'trash',
        handler: () => {
          this.roteiroService.deleteRoteiro(this.roteiro.id).then(() => {
            this.router.navigateByUrl('tabs/roteiro')
            this.messageService.showToast("Roteiro Excluido", "danger");
          });
        }
      }, {
        text: 'Editar',
        icon: 'create',
        handler: () => {
          this.router.navigate(['/roteiro-edit', this.roteiro.id], { queryParams: { "index": 0 } })
        }
      }, {
        text: 'Compartilhar',
        icon: 'share',
        handler: () => {
          console.log('Play clicked');
        }
      }, {
        text: 'Favorito',
        icon: 'heart',
        handler: () => {
          console.log('Favorite clicked');
        }
      }, {
        text: 'Cancelar',
        icon: 'close',
        role: 'cancel',
        handler: () => {
          console.log('Cancel clicked');
        }
      }]
    });
    await actionSheet.present();
  }

  async atividadePresentActionSheet(reference: string) {
    const actionSheet = await this.actionSheetController.create({
      buttons: [{
        text: 'Delete',
        role: 'destructive',
        icon: 'trash',
        handler: () => {
          this.roteiroService.deleteRoteiro(this.roteiro.id).then(() => {
            this.router.navigateByUrl('tabs/roteiro')
            this.messageService.showToast("Roteiro Excluido", "danger");
          });
        }
      }, {
        text: 'Editar',
        icon: 'create',
        handler: () => {
          this.router.navigate(['atividade-edit', this.roteiro.id], { queryParams: { "reference": reference } })
        }
      }, {
        text: 'Cancelar',
        icon: 'close',
        role: 'cancel',
        handler: () => {
          console.log('Cancel clicked');
        }
      }]
    });
    await actionSheet.present();
  }

}