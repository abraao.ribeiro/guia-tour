import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { LugarListPage } from './lugar-list.page';
import { LugaresModule } from 'src/app/shared/components/lugares/lugares.module';
import { PlacesModule } from 'src/app/shared/components/places/places.module';
const routes: Routes = [
  {
    path: '',
    component: LugarListPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    LugaresModule,
    PlacesModule,
    RouterModule.forChild(routes)
  ],
  declarations: [LugarListPage]
})
export class LugarListPageModule {}
