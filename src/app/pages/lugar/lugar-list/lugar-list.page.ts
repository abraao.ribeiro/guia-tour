import { MessageService } from 'src/app/service/message.service';
import { Component, OnInit, NgZone } from "@angular/core";
import { PopoverController } from "@ionic/angular";

import { GooglePlacesService } from "src/app/service/google-places.service";

@Component({
  selector: 'app-lugar-list',
  templateUrl: './lugar-list.page.html',
  styleUrls: ['./lugar-list.page.scss'],
})
export class LugarListPage implements OnInit {

  type: string;
  km: string = '500';
  nearbyItems: any = new Array<any>();

  constructor(
    public zone: NgZone,
    private googlePlaceService: GooglePlacesService,
    public popoverController: PopoverController,
    private messageService: MessageService
  ) {
  }

  ngOnInit() {
    this.nearbyPlace('500', '');
  }

  ionViewDidEnter() {
  }

  async nearbyPlace(km, type) {
    this.messageService.present('Carregando ....');
    this.googlePlaceService.getPlaces(km, type).then((data) => {
      this.nearbyItems = data;
      this.messageService.dismiss();
    });
  }

}