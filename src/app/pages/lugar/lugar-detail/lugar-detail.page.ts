import { MessageService } from './../../../service/message.service';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { GooglePlacesService } from 'src/app/service/google-places.service';

@Component({
  selector: 'app-lugar-detail',
  templateUrl: './lugar-detail.page.html',
  styleUrls: ['./lugar-detail.page.scss'],
})
export class LugarDetailPage implements OnInit {

  slideOpts = {
    effect: 'flip'
  };

  place: any;
  constructor(
    private activateRoute: ActivatedRoute,
    private googlePlaceService: GooglePlacesService,
    private messageService:MessageService) { }

  ngOnInit() {
    this.placeDetail();

  }
  
  placeDetail() {
    this.messageService.present('Carregando...');
    let id = this.activateRoute.snapshot.params['id'];
    this.googlePlaceService.getPlaceDetail("", id).then((placeDetail) => {
      this.place = placeDetail
      console.log("RoteiroDetalhePage", "nome:", this.place);
      this.messageService.dismiss();
    });
  }


}

