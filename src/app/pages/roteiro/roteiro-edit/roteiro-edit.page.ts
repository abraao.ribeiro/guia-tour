import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Roteiro } from 'src/app/model/roteiro';
import { Router } from '@angular/router';

import { RoteiroService } from 'src/app/service/roteiro.service';
import { MessageService } from 'src/app/service/message.service';
import { NgForm } from '@angular/forms';
@Component({
  selector: 'app-roteiro-edit',
  templateUrl: './roteiro-edit.page.html',
  styleUrls: ['./roteiro-edit.page.scss'],
})
export class RoteiroEditPage implements OnInit {
  roteiro: Roteiro = {
    titulo: '',
    descricao: '',
    atividade: []
  }

  constructor(
    private roteiroService: RoteiroService,
    private messageService: MessageService,
    private activatedRoute: ActivatedRoute,
    private router: Router) { }

  ngOnInit() {
    this.findRoteiroId();
  }
  
  ionViewWillLeave() {
  }
  findRoteiroId() {
    let id = this.activatedRoute.snapshot.params['id'];

    if (id) {
      this.roteiroService.getRoteiroId(id)
        .subscribe(r => {
          this.roteiro = r;
        });

    }
  }

  novoRoteiro() {
    this.roteiroService.criarRoteiro(this.roteiro);
    this.router.navigateByUrl('tabs/roteiro');
    this.messageService.showToast('Roteiro Adicionado', "success");
  }
  atualizarRoteiro() {
    this.roteiroService.atualizarRoteiro(this.roteiro).then(() => {
      this.router.navigateByUrl('tabs/roteiro')
      this.messageService.showToast('Roteiro atualizado com sucesso', "success");
    }, err => {
      this.messageService.showToast('Erro', "danger");
    });
  }

  excluirRoteiro() {
    this.roteiroService.deleteRoteiro(this.roteiro.id).then(() => {
      this.router.navigateByUrl('tabs/roteiro')
      this.messageService.showToast("Roteiro Excluido", "danger");
    });
  }



}
