import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';


import { IonicModule } from '@ionic/angular';
import { RoteiroListPage } from './roteiro-list.page';

const routes: Routes = [
  {
    path: '',
    component: RoteiroListPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [RoteiroListPage]
})
export class RoteiroListPageModule { }
