import { Component, OnInit } from "@angular/core";
import { ActionSheetController } from "@ionic/angular";

import { RoteiroService } from "src/app/service/roteiro.service";
import { Roteiro } from "src/app/model/roteiro";
import { MessageService } from "src/app/service/message.service";

@Component({
  selector: "app-roteiro-list",
  templateUrl: "./roteiro-list.page.html",
  styleUrls: ["./roteiro-list.page.scss"]
})
export class RoteiroListPage implements OnInit {
  shownGroup = null;
  roteiros: Roteiro[];
  roteiro: Roteiro;

  constructor(
    private roteiroService: RoteiroService,
    private messageService: MessageService,
    public actionSheetController: ActionSheetController) { }

  ngOnInit() {
    this.findAllRoteiro();
    //  this.searchByTitulo('Belem');
  }


  async findAllRoteiro() {
    this.messageService.present('Carregando Roteiros...');
    this.roteiroService.findAllRoteiro().subscribe(res => {
      this.roteiros = res;
      this.messageService.dismiss();
    });
  }


  searchByTitulo($event) {
    let value = $event.target.value
    this.roteiroService.searchRoteiroTitulo(event)
      .subscribe(result => {
        console.log(result);

      })
  }


}


