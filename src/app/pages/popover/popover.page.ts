import { PopoverController } from '@ionic/angular';
import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { MessageService } from 'src/app/service/message.service';

@Component({
  selector: 'app-popover',
  templateUrl: './popover.page.html',
  styleUrls: ['./popover.page.scss'],
})
export class PopoverPage implements OnInit {

  types: any;
  constructor(
    private httpClient: HttpClient,
    private popoverController: PopoverController,
    private messaggeService:MessageService) { }

  ngOnInit() {
    this.getCategorias();
  }

  getCategorias() {
    this.httpClient.get('assets/data/lugares-type.json')
      .subscribe(data => {
        this.types = data;
      });
  }

  fechaPopover(km: any, type: any) {
    this.popoverController.dismiss({
      'result': km, type
    });
  }



}

