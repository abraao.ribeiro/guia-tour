import { Atividade } from "./atividade";
export class Roteiro {
  id?: string;
  userId?: string;
  titulo?: string;
  descricao?: string;
  atividade?: Atividade[];
}
