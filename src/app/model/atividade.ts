export class Atividade {
        id?:string;
        reference?:string;
        img?:string;
        lugar?: string;
        data?: Date;
        horaEntrada?: Date;
        horaSaida?: Date;
        lat?:string;
        lng?:string;
}
